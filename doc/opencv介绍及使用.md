[TOC]

# Opencv介绍及使用

> Windows下32位Qt Mingw32编译
>
> 官网：<https://opencv.org/>
>
> Opencv是一个基于BSD开源许可发行的跨平台计算机视觉库，可以运行在Linux、Windows、Android、Maemo、FreeBSD、OpenBSD、IOS、Mac OS等平台上运行，OpenCV用C++语言编写，它的主要接口也是C++语言，但是依然保留了大量的C语言接口。也有大量的Python、Java和MATLAB/OCTAVE的接口。

## OpenCV的 MSVC 版及 MinGW 版

下载的Opencv文件夹会有：

- build（已编译好的库）
- sources（源码）

使用 MSVC 的话，直接在

​	```build/x64/vc14``` 里面就有了，配置好路径就可以使用。

使用 MinGW 版

​	opencv没有为我们编译好 MinGW 版，需要我们自己下载opencv源码编译。

## opencv源码

​	opencv for windows 跟 opencv for linux 的源码是一样的，只是 opencv for windows 里面多了已编译好的 opencv。

## Qt编译Opencv

1. 下载 QtCreator
2. 下载 CMake <https://cmake.org/download/> 如果QT的mingw编译器是32位`MinGW 5.3.0 32bit`的，则CMake也要下载相对应 32 位版本，否则编译通不过。
3. 安装CMake时勾选自动配置环境变量。在QtCreator下的：选项--构建与运行--CMake--会自动检测出cmake。

   - ![1544521908793](assets/1544521908793.png)

4. 下载Opencv一个版本的源码，用QTCreator打开OpenCV源码中的 `CMakeLists.txt`，就像加载.pro一样加载opencv工程，进入项目，编译构造配置选择 Release，等待几分钟，Qt正在加载 CMakeLists.txt 文件，具体过程可以从QtCreator中的概要信息中看到。加载完毕后，需要对配置做一些修改。

   - ![1544521952282](assets/1544521952282.png)

   - ![1544524861487](assets/1544524861487.png)

5. 修改配置，刚加载上工程配置会慢一些：

   - ![1544525319601](assets/1544525319601.png)

   - 勾选`WITH_OPENGL`、`WITH_QT`

   - 取消勾选`WITH_IPP`、`WITH_MSMF` `ENABLE_PRECOMPILED_HEADERS`。

   - 设置`CMAKE_INSTALL_PREFIX`一个路径，这个路径是编译完成后的输出路径。**双击-浏览选路径，选择完毕后，回车确认，否则可能选不上**。 

   - 再到构建步骤，详情中勾选 install。**注意：**只有勾选了此选项后，编译完成后的输出路径才有效。

   - 点击按钮 Apply Configuration Changes，完成后，点击构建即可。构建过程中如果想要用多个核同时编译，可以设置qt的 Environmeng: MAKEFLAGS=-j8，如下图：

   - ![1544526734267](assets/1544526734267.png)

   - ![1544533529280](assets/1544533529280.png)

   - 编译时出现错误：

     - ```
       在文件opencv/sources/modules/videoio/src/cap_dshow.cpp中出错 error : 'sprintf_instead_use_StringCbPrintfA_or_StringCchPrintfA' was not declared in this scope ...
       
       打开cap_dshow.cpp 在如下位置增加 #define NO_DSHOW_STRSAFE 继续编译即可。如上如即可。
       #include <vector>
       
       //Include Directshow stuff here so we don't worry about needing all the h files.
       #define NO_DSHOW_STRSAFE
       #include "DShow.h"
       #include "strmif.h"
       #include "Aviriff.h"
       #include "dvdmedia.h"
       #include "bdaiface.h"
       ```

## Qt加载Opencv

注意设置Opencv的bin目录到环境变量中

```
.pro

win32 {
    INCLUDEPATH += C:\OpenCV\4.0.0\include
    LIBS += C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_calib3d400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_core400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_dnn400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_features2d400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_flann400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_gapi400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_highgui400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_imgcodecs400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_imgproc400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_ml400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_objdetect400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_photo400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_stitching400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_video400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_videoio400.dll.a
}
```

```
#include "mainwindow.h"
#include <QApplication>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
using namespace cv;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    Mat img = imread(QString("1.jpg").toStdString());
    namedWindow("111");
    imshow("111", img);

    return a.exec();
}
```

