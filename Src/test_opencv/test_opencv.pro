#-------------------------------------------------
#
# Project created by QtCreator 2018-12-11T20:48:17
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_opencv
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui

win32 {
    INCLUDEPATH += C:\OpenCV\4.0.0\include
    LIBS += C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_calib3d400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_core400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_dnn400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_features2d400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_flann400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_gapi400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_highgui400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_imgcodecs400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_imgproc400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_ml400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_objdetect400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_photo400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_stitching400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_video400.dll.a \
            C:\OpenCV\4.0.0\x86\mingw\lib\libopencv_videoio400.dll.a
}

CONFIG(debug, debug|release){
    DESTDIR = ../bin
}
else {
    DESTDIR = ../bin
}
