﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //opencv camera
    VideoCapture m_VideoCapture;
    QTimer m_timer;
    bool m_bInterupt;

    QImage cvMat2QImage(const cv::Mat &mat);
    bool GetcvMatFrame(cv::Mat &mat);
public slots:
    void slotTimeOut();
private slots:
    void on_btnOpenCamera_clicked();
    void on_btnGetFrameShow_clicked();
    void on_btnAlwaysShow_clicked();
    void on_btnStopAlwaysShow_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
